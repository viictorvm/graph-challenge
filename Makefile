DOCKER_SERVICE=slim

.PHONY: start
start: stop erase build up

copy:
	pip freeze > requirements.txt

.PHONY: up
up:
		UID=${UID} GID=${GID} docker-compose up -d

.PHONY: stop
stop:
		docker-compose stop

.PHONY: erase
erase:
		docker-compose down -v

.PHONY: build
build:
		docker-compose build && \
		docker-compose pull

.PHONY: bash
bash:
		docker exec -it  ${DOCKER_SERVICE} sh