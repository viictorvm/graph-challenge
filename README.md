# Graph Challenge
API made using [DDD](https://en.wikipedia.org/wiki/Domain-driven_design).

Stack:
1. [Slim](http://www.slimframework.com/) Framework.
2. [MongoDB](https://www.mongodb.com/) as Database.

Database:
We have two collection for represent our graph. Nodes and Edges (the relation between two nodes).

Nodes
```json
{
    "id": 7,
    "name": "7name"
}
```

Edges
```json
{
    "id": 7,
    "child": "4,10,9"
}
```

## Use
For build our dockers on localhost or on deploy, we created a Makefile file with the common docker commands.
+ Copy .env.dist to .env
+ To build and run dockers we have to execute: `make start`
+ To stop these containers: `make stop`

## Containers

1. For our app we will use PHP 7.3 where we will install Slim.
2. MongoDB image.

## Endpoints
1. `/graphs` for everything related with Edges (relation between two nodes).
2. `/nodes` for everything relate with Nodes. 

For example, for create a new node we will go to: `/nodes/{new_id}`
with a JSON body with all the metadata. Until now, we just have **name** field, but it's ready
for future metadata.
`{
    "name": "45name"
}`

## The Future of our App Development.
* Apply Swagger API
* Continuous Delivery using [GitLab CI/CD](https://about.gitlab.com/product/continuous-integration/) native facilities.
* [Ansible](https://www.ansible.com/) Script for the automation and provisioning.

## Problem
The goal is to set up a web service that provides an API 
for clients to run algorithms and perform updates on the graphs. 
The graphs it will deal with are normal, directed graphs with a set of nodes and edges. 
There can be cycles in the graph. Each node and edge should have a primary ID and additional metadata like a name and other attributes. 
The web service needs functions to persist and query the basic graph structure.

![Graph example](graph.png)

*Author: Víctor Vallecillo Morilla email: viictorvallecillo@gmail.com*


