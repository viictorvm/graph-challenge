<?php
declare(strict_types=1);

use App\Domain\Graph\GraphRepository;
use App\Domain\Node\NodeRepository;
use App\Infrastructure\Persistence\Graph\GraphMongoRepository;
use App\Infrastructure\Persistence\Node\NodeMongoRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        GraphRepository::class => \DI\autowire(GraphMongoRepository::class),
        NodeRepository::class => \DI\autowire(NodeMongoRepository::class),
    ]);
};
