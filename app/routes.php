<?php
declare(strict_types=1);

use App\Application\Actions\Graph\AddGraphAction;
use App\Application\Actions\Graph\ListGraphAction;
use App\Application\Actions\Graph\UpdateGraphAction;
use App\Application\Actions\Graph\ViewGraphAction;
use App\Application\Actions\Node\DeleteNodeAction;
use App\Application\Actions\Node\ListNodeAction;
use App\Application\Actions\Node\PersistNodeAction;
use App\Application\Actions\Node\ViewNodeAction;
use App\Application\Actions\ShortestPath\ResolveGraphAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $container = $app->getContainer();

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');
        return $response;
    });

    $app->group('/graphs', function (Group $group) use ($container) {
        $group->get('', ListGraphAction::class);
        $group->patch('/{id}', UpdateGraphAction::class);
        $group->post('/{id}', AddGraphAction::class);
        $group->get('/{id}', ViewGraphAction::class);
    });

    $app->group('/nodes', function (Group $group) use ($container) {
        $group->get('', ListNodeAction::class);
        $group->get('/{id}', ViewNodeAction::class);
        $group->post('/{id}', PersistNodeAction::class);
        $group->delete('/{id}', DeleteNodeAction::class);
    });
};
