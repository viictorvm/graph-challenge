<?php
declare(strict_types=1);

namespace App\Application\Actions\Graph;

use App\Domain\Edge\Edge;
use Psr\Http\Message\ResponseInterface as Response;

class AddGraphAction extends GraphAction
{
    protected function action(): Response
    {
        $idNode = (int)$this->resolveArg('id');
        $child = $this->getFormData()->child;
        $edgeObject = new Edge($idNode, $child);
        $graphs = $this->graphRepository->persist($edgeObject);
        $this->logger->info("Graph of id " . $idNode . "was inserted.");

        return $this->respondWithData($graphs);
    }
}
