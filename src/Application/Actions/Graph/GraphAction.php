<?php
declare(strict_types=1);

namespace App\Application\Actions\Graph;

use App\Application\Actions\Action;
use App\Domain\Graph\GraphRepository;
use Psr\Log\LoggerInterface;

abstract class GraphAction extends Action
{
    protected $graphRepository;

    public function __construct(LoggerInterface $logger, GraphRepository $graphRepository)
    {
        parent::__construct($logger);
        $this->graphRepository = $graphRepository;
    }
}
