<?php
declare(strict_types=1);

namespace App\Application\Actions\Graph;

use Psr\Http\Message\ResponseInterface as Response;

class ListGraphAction extends GraphAction
{
    protected function action(): Response
    {
        $graphs = $this->graphRepository->findAll();
        $this->logger->info("Graph list was viewed.");

        return $this->respondWithData($graphs);
    }
}
