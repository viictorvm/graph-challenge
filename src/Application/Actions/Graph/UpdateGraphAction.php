<?php
declare(strict_types=1);

namespace App\Application\Actions\Graph;

use App\Domain\Edge\Edge;
use App\Domain\Graph\GraphNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;

class UpdateGraphAction extends GraphAction
{
    protected function action(): Response
    {
        $idNode = (int)$this->resolveArg('id');
        $child = $this->getFormData()->child;
        $existingNode = $this->graphRepository->findEdgeById($idNode);

        if (!$existingNode) {
            throw new GraphNotFoundException();
        }
        $edge = new Edge($idNode, $child);
        $graphs = $this->graphRepository->updateEdge($edge);
        $this->logger->info("Graph of id " . $idNode . "was updated.");

        return $this->respondWithData($graphs);
    }
}
