<?php
declare(strict_types=1);

namespace App\Application\Actions\Graph;

use Psr\Http\Message\ResponseInterface as Response;

class ViewGraphAction extends GraphAction
{
    protected function action(): Response
    {
        $idNode = (int)$this->resolveArg('id');
        $graph = $this->graphRepository->findEdgeById($idNode);
        $this->logger->info("Edge of id" . $idNode . " was viewed.");

        return $this->respondWithData($graph);
    }
}
