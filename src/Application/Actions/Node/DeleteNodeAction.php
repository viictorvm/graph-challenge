<?php
declare(strict_types=1);

namespace App\Application\Actions\Node;

use App\Domain\Node\NodeFoundException;
use Psr\Http\Message\ResponseInterface as Response;

class DeleteNodeAction extends NodeAction
{
    protected function action(): Response
    {
        $idNode = (int)$this->resolveArg('id');
        $node = $this->nodeRepository->findNodeById($idNode);
        if (NULL === $node) {
            throw new NodeFoundException();
        }
        $result = $this->nodeRepository->delete($node);
        $this->logger->info("Node of id" . $idNode . " was deleted.");

        return $this->respondWithData($result);
    }
}
