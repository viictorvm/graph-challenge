<?php
declare(strict_types=1);

namespace App\Application\Actions\Node;

use Psr\Http\Message\ResponseInterface as Response;

class ListNodeAction extends NodeAction
{
    protected function action(): Response
    {
        $graphs = $this->nodeRepository->findAll();
        $this->logger->info("Node list was viewed.");

        return $this->respondWithData($graphs);
    }
}
