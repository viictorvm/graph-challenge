<?php
declare(strict_types=1);

namespace App\Application\Actions\Node;

use App\Application\Actions\Action;
use App\Domain\Node\NodeRepository;
use Psr\Log\LoggerInterface;

abstract class NodeAction extends Action
{
    protected $nodeRepository;

    public function __construct(LoggerInterface $logger, NodeRepository $nodeRepository)
    {
        parent::__construct($logger);
        $this->nodeRepository = $nodeRepository;
    }
}
