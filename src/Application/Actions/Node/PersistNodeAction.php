<?php
declare(strict_types=1);

namespace App\Application\Actions\Node;

use App\Domain\Node\Node;
use Psr\Http\Message\ResponseInterface as Response;

class PersistNodeAction extends NodeAction
{
    protected function action(): Response
    {
        $idNode = (int)$this->resolveArg('id');
        $metadata = (array)$this->getFormData();
        $node = new Node($idNode, $metadata);
        $result = $this->nodeRepository->persist($node);
        $this->logger->info("Node of id" . $idNode . " was inserted.");

        return $this->respondWithData($result);
    }
}
