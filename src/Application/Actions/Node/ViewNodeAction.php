<?php
declare(strict_types=1);

namespace App\Application\Actions\Node;

use Psr\Http\Message\ResponseInterface as Response;

class ViewNodeAction extends NodeAction
{
    protected function action(): Response
    {
        $idNode = (int)$this->resolveArg('id');
        $graph = $this->nodeRepository->findNodeById($idNode);
        $this->logger->info("Node of id" . $idNode . " was viewed.");

        return $this->respondWithData($graph);
    }
}
