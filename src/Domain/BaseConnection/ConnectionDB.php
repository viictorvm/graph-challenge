<?php

namespace App\Domain\BaseConnection;

interface ConnectionDB
{
    public static function connection();
}