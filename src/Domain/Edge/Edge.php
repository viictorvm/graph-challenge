<?php
declare(strict_types=1);

namespace App\Domain\Edge;

use JsonSerializable;

class Edge implements JsonSerializable
{

    private $id;
    private $child;
    private $deleted;

    public function __construct(int $id, array $child)
    {
        $this->id = $id;
        $this->child = $child;
        $this->deleted = false;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function child(): array
    {
        return $this->child;
    }

    public function isDeleted()
    {
        return $this->deleted;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id(),
            'child' => $this->child(),
            'deleted' => $this->isDeleted()
        ];
    }
}
