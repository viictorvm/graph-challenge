<?php
declare(strict_types=1);

namespace App\Domain\Graph;

use JsonSerializable;
use SplQueue;

class Graph implements JsonSerializable
{
    private $data = [];

    public function __construct($graphData)
    {
        $this->data = $this->parseDataToDict($graphData);
    }

    public function data(): array
    {
        return $this->data;
    }

    public function findShortestPath(int $idSrc, int $idDst)
    {
        $visited = [];
        $found = false;
        $queue = new SplQueue();
        $queue->enqueue($idSrc);
        $this->findPath($idDst, $found, $queue, $visited);

        if (!$found) {
            throw new PathNotFoundException();
        }

        return $visited;
    }

    private function parseDataToDict($graphData)
    {
        if (empty($graphData)) {
            throw new GraphNotFoundException();
        }
        $data = [];
        foreach ($graphData as $row) {
            $data[$row->id] = $row->child;
        }

        return $data;
    }

    public function jsonSerialize()
    {
        return [
            'graph' => $this->data,
        ];
    }
}
