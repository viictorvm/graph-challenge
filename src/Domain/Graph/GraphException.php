<?php
declare(strict_types=1);

namespace App\Domain\Graph;

use App\Domain\DomainException\DomainRecordNotFoundException;

class GraphException extends DomainRecordNotFoundException
{
    public $message = 'Something went wong with the collection"';
}
