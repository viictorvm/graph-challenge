<?php
declare(strict_types=1);

namespace App\Domain\Graph;

use App\Domain\DomainException\DomainRecordNotFoundException;

class GraphNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The graph you requested does not exist.';
}
