<?php
declare(strict_types=1);

namespace App\Domain\Graph;

use App\Domain\Edge\Edge;

interface GraphRepository
{
    public function findAll(): Graph;
    public function findEdgeById(int $idNode);
    public function updateEdge(Edge $edge);
    public function persist(Edge $edgeObject);
}
