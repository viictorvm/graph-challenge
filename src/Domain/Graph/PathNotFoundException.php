<?php
declare(strict_types=1);

namespace App\Domain\Graph;

use App\Domain\DomainException\DomainRecordNotFoundException;

class PathNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'Path not found! Please, check the nodes';
}
