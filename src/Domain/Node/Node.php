<?php
declare(strict_types=1);

namespace App\Domain\Node;

use JsonSerializable;

class Node implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    private $deleted;

    /**
     * @param int $id
     * @param array $metadata
     */
    public function __construct(int $id = 0, array $metadata = [])
    {
        $this->create($id, $metadata);
    }


    /**
     * @param int $id
     * @param array $metadata
     */
    private function create(int $id, array $metadata)
    {
        $this->id = $id;
        $this->name = $metadata ? $metadata['name'] : '';
        $this->deleted = false;
    }

    /**
     * @param Object $obj
     * @return static
     */
    public static function createByObject(Object $obj)
    {
        return new static($obj->id, json_decode(json_encode($obj), true));
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return strtolower($this->name);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'deleted' => $this->deleted
        ];
    }
}
