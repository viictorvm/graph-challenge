<?php
declare(strict_types=1);

namespace App\Domain\Node;

use App\Domain\DomainException\DomainRecordNotFoundException;

class NodeExecuteQueryException extends DomainRecordNotFoundException
{
    public $message = 'The current query is not available';
}
