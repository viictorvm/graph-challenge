<?php
declare(strict_types=1);

namespace App\Domain\Node;

use App\Domain\DomainException\DomainRecordNotFoundException;

class NodeFoundException extends DomainRecordNotFoundException
{
    public $message = 'The node you requested already exist.';
}
