<?php
declare(strict_types=1);

namespace App\Domain\Node;

interface NodeRepository
{
    public function findAll(): array;
    public function findNodeById(int $idNode);
    public function persist(Node $node);
    public function delete(Node $node);
}
