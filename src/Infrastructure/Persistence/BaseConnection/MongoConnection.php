<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\BaseConnection;

use App\Domain\BaseConnection\ConnectionDB;
use MongoDB;

class MongoConnection implements ConnectionDB
{
    public static function connection()
    {
        return new MongoDB\Driver\Manager('mongodb://mongodb:27017');
    }
}