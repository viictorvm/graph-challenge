<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Graph;

use App\Domain\Edge\Edge;
use App\Domain\Graph\Graph;
use App\Domain\Graph\GraphException;
use App\Domain\Graph\GraphFoundException;
use App\Domain\Graph\GraphRepository;
use App\Infrastructure\Persistence\BaseConnection\MongoConnection;
use MongoDB;

class GraphMongoRepository implements GraphRepository
{
    private $MONGO_COLLECTION = "graph.edge";
    private $db;

    public function __construct()
    {
        $this->db = MongoConnection::connection();
    }

    public function findAll(): Graph
    {
        $query = new MongoDB\Driver\Query(['deleted' => false]);
        $result = $this->executeQuery($query);

        if(empty($result)) {
            throw new GraphException();
        }

        return new Graph($result);
    }

    public function findEdgeById(int $idNode)
    {
        $query = new MongoDB\Driver\Query(['id' => $idNode, 'deleted' => false], ['projection' => ['_id' => 0]]);
        $result = $this->executeQuery($query);

        return $this->parseToObject($result);
    }

    public function updateEdge(Edge $edge)
    {
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->update(
            ['id' => $edge->id(), 'deleted' => false],
            ['$set' => ['child' => $edge->child()]]
        );
        $this->executeBulk($bulk);

        return $this->findEdgeById($edge->id());
    }

    private function executeQuery(MongoDB\Driver\Query $query)
    {
        $result = '';
        try {
            $result = $this->db->executeQuery($this->MONGO_COLLECTION, $query);
        } catch (MongoDB\Driver\Exception\Exception $e) {
            print($e);
        }

        return $result;
    }

    private function executeBulk(MongoDB\Driver\BulkWrite $bulk)
    {
        $this->db->executeBulkWrite($this->MONGO_COLLECTION, $bulk);
    }

    private function parseToObject($dataToParse)
    {
        $graphDict = [];
        foreach ($dataToParse as $row) {
            $graphDict[$row->id] = $row->child;
        }

        return $graphDict;
    }

    private function checkIsCreated(int $idNode): void
    {
        $existingGraph = $this->findEdgeById($idNode);
        if (!empty($existingGraph)) {
            throw new GraphFoundException();
        }
    }

    public function persist(Edge $edgeObject)
    {
        $bulk = new MongoDB\Driver\BulkWrite;
        $this->checkIsCreated($edgeObject->id());
        $bulk->insert($edgeObject->jsonSerialize());
        $this->executeBulk($bulk);

        return $this->findAll();
    }
}
