<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Node;

use App\Domain\Node\Node;
use App\Domain\Node\NodeExecuteQueryException;
use App\Domain\Node\NodeFoundException;
use App\Domain\Node\NodeRepository;
use App\Infrastructure\Persistence\BaseConnection\MongoConnection;
use MongoDB;

class NodeMongoRepository implements NodeRepository
{
    private $MONGO_COLLECTION = "graph.node";
    private $db;

    public function __construct()
    {
        $this->db = MongoConnection::connection();
    }

    public function findAll(): array
    {
        $query = new MongoDB\Driver\Query(['deleted' => false]);
        $result = $this->executeQuery($query);

        return $this->parseToDict($result);
    }

    public function findNodeById(int $idNode)
    {
        $node = NULL;
        $query = new MongoDB\Driver\Query(['id' => $idNode, 'deleted' => false], ['projection' => ['_id' => 0]]);
        $result = $this->executeQuery($query)->toArray();
        if ($result) {
            $nodeData = $result[0];
            $node = Node::createByObject($nodeData);
        }

        return $node;
    }

    private function executeQuery(MongoDB\Driver\Query $query)
    {
        try {
            $result = $this->db->executeQuery($this->MONGO_COLLECTION, $query);
        } catch (MongoDB\Driver\Exception\Exception $e) {
            throw new NodeExecuteQueryException();
        }

        return $result;
    }

    private function parseToDict($dataToParse)
    {
        $graphDict = [];
        foreach ($dataToParse as $row) {
            $graphDict[] = $row->id;
        }

        return $graphDict;
    }

    public function persist(Node $node)
    {
        $bulk = new MongoDB\Driver\BulkWrite;
        $this->checkIsCreated($node->id());
        $bulk->insert($node->jsonSerialize());
        $this->executeBulk($bulk);

        return $this->findNodeById($node->id());
    }

    private function executeBulk(MongoDB\Driver\BulkWrite $bulk)
    {
        $this->db->executeBulkWrite($this->MONGO_COLLECTION, $bulk);
    }

    private function checkIsCreated(int $idNode): void
    {
        $existingNode = $this->findNodeById($idNode);
        if (!empty($existingNode)) {
            throw new NodeFoundException();
        }
    }

    public function delete(Node $node)
    {
        $bulk = new MongoDB\Driver\BulkWrite;
        $this->checkIsCreated($node->id());
        $bulk->update(
            ['id' => $node->id()],
            ['$set' => ['deleted' => true]]
        );
        $this->executeBulk($bulk);
    }
}
